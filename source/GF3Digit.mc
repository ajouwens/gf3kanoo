using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Math as Math;

module GF3Helper {

    function drawDate(dc, info, posX, posY) {
        var decDay = info.day % 10;
        var decMon = info.month % 10;
        dc.setColor(colorOn, Gfx.COLOR_TRANSPARENT);
        dc.drawRoundedRectangle(posX+16, posY-32, 65, 24, 3);
        var color = colorOn;
        if(info.day < 10) {
            drawDigit(dc, posX+19, posY-30, 0, color);
            drawDigit(dc, posX+34, posY-30, decDay, color);
        } else {
            drawDigit(dc, posX+19, posY-30, (info.day-decDay)/10, color);
            drawDigit(dc, posX+34, posY-30, decDay, color);
        }
        dc.fillRectangle(posX+47, posY-13, 3, 3);
        if(info.month < 10) {
            drawDigit(dc, posX+51, posY-30, 0, color);
            drawDigit(dc, posX+66, posY-30, decMon, color);
        } else {
            drawDigit(dc, posX+51, posY-30, (info.month-decMon)/10, color);
            drawDigit(dc, posX+66, posY-30, decMon, color);
        }
    }

    var segs=[
        [1,0,1,1,1,1,1],    //0
        [0,0,0,0,0,1,1],    //1
        [1,1,1,0,1,1,0],    //2
        [1,1,1,0,0,1,1],    //3
        [0,1,0,1,0,1,1],    //4
        [1,1,1,1,0,0,1],    //5
        [1,1,1,1,1,0,1],    //6
        [1,0,0,0,0,1,1],    //7
        [1,1,1,1,1,1,1],    //8
        [1,1,1,1,0,1,1]];   //9

    function drawDigit(dc,x,y,dig, color) {
        var w   = 12;
        var h   = 18;
        var t   = 2;
        var hh  = h/2;
        var ht  = t/2;
        var qt  = (t < 4) ? 1 : t/4;
        var xt  = x+t;
        var xwt = x+w-t;
        var xw  = x+w;

        dc.setColor(color, Gfx.COLOR_TRANSPARENT);
        if(segs[dig][0]) { // Top horizontal bar
            var pts = [[x,y],[xw,y],[xwt,y+t],[xt,y+t]];
            dc.fillPolygon(pts);
        }
        if(segs[dig][1]) { // Middle horizontal bar
            var pts = [[x,y+hh+ht],[xt,y+hh],[xwt,y+hh],[xw,y+hh+ht],[xwt,y+hh+t],[xt,y+hh+t]];
            dc.fillPolygon(pts);
        }
        if(segs[dig][2]) { // Bottom horizontal bar
            var pts = [[x,y+h+t],[xt,y+h],[xwt,y+h],[xw,y+h+t]];
            dc.fillPolygon(pts);
        }
        if(segs[dig][3]) { // Top left vertical bar
            var pts = [[x,y+qt],[xt,y+t+qt],[xt,y+hh-qt],[x,y+hh+qt]];
            dc.fillPolygon(pts);
        }
        if(segs[dig][4]) { // Bottom left vertical bar
            var pts = [[x,y+hh+ht+qt],[xt,y+hh+t+qt],[xt,y+h-qt],[x,y+h+ht+qt]];
            dc.fillPolygon(pts);
        }
        if(segs[dig][5]) { // Top right vertical bar
            var pts = [[xw,y+qt],[xw,y+hh+qt],[xwt,y+hh-qt],[xwt,y+t+qt]];
            dc.fillPolygon(pts);
        }
        if(segs[dig][6]) { // Bottom right vertical bar
            var pts = [[xw,y+hh+ht+qt],[xw,y+h+ht+qt],[xwt,y+h-qt],[xwt,y+hh+t+qt]];
            dc.fillPolygon(pts);
        }
    }

}