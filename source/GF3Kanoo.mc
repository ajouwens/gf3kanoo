using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Math;
using Toybox.Time as Time;
using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;
using Toybox.ActivityMonitor as Act;

class GF3Kanoo extends Ui.WatchFace {
    var pi24 = Math.PI / 2.4;
    var pi6  = Math.PI / 6.0;
    var colorBg = Gfx.COLOR_BLACK;
    var colorFg = Gfx.COLOR_WHITE;
    var colorAc = Gfx.COLOR_RED;
    var is24Hour = false;
    var minuteBarText = {:one => "15", :two => "30", :tri => "45"};
    var powerBarText  = {:one => "25", :two => "50", :tri => "75"};
    var amPmBtBarText = {:one => ["AM", 0, 13], :two => ["PM", 25, 13], :tri => ["BT", 50, 13]};
    var logo = [[0,0],[2,0],[2,3],[5,0],[7,0],[3,4],[6,7],[13,0],[15,0],[21,6],[21,0],[23,0],[29,6],[29,0],[31,0],[31,8],[29,8],[23,2],[23,8],[21,8],[14,1],[7,8],[5,8],[2,5],[2,8],[0,8]];

    var cx;
    var cy;
    var roundWatch = true;

    function initialize() {
        is24Hour = Sys.getDeviceSettings().is24Hour;
    }

    function onLayout(dc) {
        cx = dc.getWidth() / 2;
        cy = dc.getHeight() / 2;
        roundWatch = (cx.toDouble() /cy.toDouble() == 1.0d);
    }

    function onUpdate(dc) {
        dc.setColor(colorBg, colorBg, colorBg);
        dc.clear();
        var clockTime = System.getClockTime();
        var hour= clockTime.hour;
        var min = clockTime.min;
        var now = Time.now();
        var battPerc = (Sys.getSystemStats().battery).toDouble();
        var stepGoal = (Act.getInfo().stepGoal).toDouble();
        var steps = (Act.getInfo().steps).toDouble();
        var moveBar = (Act.getInfo().moveBarLevel).toDouble();

        var hourUI = GF3Helper.getHourUI(hour, false);
        for (var i = 0; i < 12; i += 1) {
            drawHourDot(dc, (i * pi6) + pi24, -30, i == (hourUI[0] - 1) ? colorAc : colorFg);
        }

        // Steps & Step Goal
        if(stepGoal != null && stepGoal > 0.0){
            var relPos = steps <=  stepGoal ? (steps / stepGoal) * 100.0 : 100.0;
            var stepsDone = (steps > stepGoal) ? steps : stepGoal;
            drawStepGoal(dc, relPos, 70, 12, cx+20, cy-60, stepsDone);
            drawMoveDots(dc, moveBar, roundWatch ? 60 : 75, cx+25, cy-64);
        } else {
            drawLogo(dc, cx+20, cy-66, logo, 1d);
        }

        drawAmPmBt(dc, hourUI, cx+31, cy+10, amPmBtBarText);
        drawMinuteBar(dc, min, 180, 16, cx-90, cy+35, minuteBarText);
        drawPowerBar(dc, battPerc, 70, 12, cx+20, cy-27, powerBarText);
        drawDate(dc, cx-40, cy-30);
    }

    function drawLogo(dc, posX, posY, coords) {
        var result = new [coords.size()];
        for (var i = 0; i < coords.size(); i += 1) {
            var x = coords[i][0] + posX;
            var y = coords[i][1] + posY;
            result[i] = [x, y];
        }
        dc.setColor(colorAc, Gfx.COLOR_TRANSPARENT);
        dc.fillPolygon(result);
        dc.fillEllipse(posX+37, posY+4, 6, 4);
        dc.fillEllipse(posX+47, posY+4, 6, 4);
        dc.setColor(colorBg, Gfx.COLOR_TRANSPARENT);
        dc.fillEllipse(posX+37, posY+4, 4, 2);
        dc.fillEllipse(posX+47, posY+4, 4, 2);
    }

    function drawHourDot(dc, angle, position, color) {
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);
        var x = (position * cos) - (position * sin) + cx-38;
        var y = (position * sin) + (position * cos) + cy-20;
        dc.setColor(color, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(x, y, 8);
    }

    function drawDate(dc, posX, posY) {
        var now = Time.now();
        var info = Calendar.info(now, Time.FORMAT_SHORT);
        var info2 = Calendar.info(now, Time.FORMAT_MEDIUM);

        var dateStr;
        if (is24Hour) {
            dateStr = Lang.format("$1$ / $2$", [info.day, info.month]);
        } else {
            dateStr = Lang.format("$1$ / $2$", [info.month, info.day]);
        }
        dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
        dc.drawText(posX, posY, Gfx.FONT_TINY, Lang.format("$1$", [info2.day_of_week]), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(posX, posY+17, Gfx.FONT_TINY, dateStr, Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
    }

    //! value  - the value to draw.
    //! width  - the width of the bar.
    //! height - the height of the bar.
    //! posX   - the x coordinate of the top left position.
    //! posY   - the y coordinate of the top left position.
    //! text   - the text to draw on positions: 1/4, 1/2 and 3/4 of the bar.
    function drawMinuteBar(dc, value, width, height, posX, posY, text) {
        dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(posX, posY, width, height);
        dc.setColor(colorAc, Gfx.COLOR_TRANSPARENT);
        for (var i = 0; i <= value*3; i += 2) {
            dc.drawLine(posX+i, posY, posX+i, posY+height);
        }
        // draw the markers
        dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
        var yMarkTop    = posY+height+2;
        var yMarkBottom = posY+height+5;
        for (var i = 0; i <= width; i += 15) {
            dc.drawLine(posX+i, yMarkTop, posX+i, yMarkBottom);
        }
        var yText = posY+29;
        dc.drawText(posX+(1*width/4), yText, Gfx.FONT_XTINY, text.get(:one), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(posX+(2*width/4), yText, Gfx.FONT_XTINY, text.get(:two), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(posX+(3*width/4), yText, Gfx.FONT_XTINY, text.get(:tri), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
    }

    function drawAmPmBt(dc, hourUI, posX, posY, text) {
        var oneXY = [posX+text.get(:one)[1], posY+text.get(:one)[2]];
        var twoXY = [posX+text.get(:two)[1], posY+text.get(:two)[2]];
        var triXY = [posX+text.get(:tri)[1], posY+text.get(:tri)[2]];

        dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
        dc.drawText(oneXY[0], oneXY[1], Gfx.FONT_TINY, text.get(:one)[0], Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(twoXY[0], twoXY[1], Gfx.FONT_TINY, text.get(:two)[0], Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(triXY[0], triXY[1], Gfx.FONT_TINY, text.get(:tri)[0], Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);

        var colors;
        if("AM".equals(hourUI[1])) {
            colors = {:am => colorAc, :pm => colorFg};
        } else {
            colors = {:am => colorFg, :pm => colorAc};
        }
        // AM
        dc.setColor(colors.get(:am), Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(oneXY[0], oneXY[1]-12, 4);
        // PM
        dc.setColor(colors.get(:pm), Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(twoXY[0], twoXY[1]-12, 4);
        // BT
        if(Sys.getDeviceSettings().phoneConnected == true) {
            dc.setColor(colorAc, Gfx.COLOR_TRANSPARENT);
        } else {
            dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
        }
        dc.fillCircle(triXY[0], triXY[1]-12, 4);
    }

    function drawPowerBar(dc, value, width, height, posX, posY, text) {
        var perc = width/100.0;
        dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(posX, posY, width, height);
        dc.setColor(colorAc, Gfx.COLOR_TRANSPARENT);
        for (var i = 0; i <= value*perc; i += 2) {
            dc.drawLine(posX+i, posY, posX+i, posY+height);
        }
        // draw the markers
        dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
        var yMarkTop    = posY+height+2;
        var yMarkBottom = posY+height+5;
        for (var i = 0; i <= width; i += (25*perc)) {
            dc.drawLine(posX+i, yMarkTop, posX+i, yMarkBottom);
        }
        var yText = posY+23;
        dc.drawText(posX+(1*width/4), yText, Gfx.FONT_XTINY, text.get(:one), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(posX+(2*width/4), yText, Gfx.FONT_XTINY, text.get(:two), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(posX+(3*width/4), yText, Gfx.FONT_XTINY, text.get(:tri), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
    }

    function drawMoveDots(dc, move, width, posX, posY) {
        dc.setColor(colorAc, Gfx.COLOR_TRANSPARENT);
        var space = width/5;
        for (var i = 0; i < 5; i += 1) {
            if (i >= move) {
                dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
            }
            dc.fillCircle(posX+(i*space), posY-5, 4);
        }
    }

    function drawStepGoal(dc, value, width, height, posX, posY, text) {
        var perc = width/100.0;
        dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
        dc.fillRectangle(posX, posY, width, height);
        dc.setColor(colorAc, Gfx.COLOR_TRANSPARENT);
        for (var i = 0; i <= value*perc; i += 2) {
            dc.drawLine(posX+i, posY, posX+i, posY+height);
        }
        // draw the markers
        dc.setColor(colorFg, Gfx.COLOR_TRANSPARENT);
        var yMarkTop    = posY+height+2;
        var yMarkBottom = posY+height+5;
        for (var i = 0; i <= width; i += (25*perc)) {
            dc.drawLine(posX+i, yMarkTop, posX+i, yMarkBottom);
        }
        dc.drawText(posX+70, posY+23, Gfx.FONT_XTINY, text.format("%d"), Gfx.TEXT_JUSTIFY_RIGHT | Gfx.TEXT_JUSTIFY_VCENTER);
    }

}
