using Toybox.Application as App;

class GF3KanooWatch extends App.AppBase {
    function onStart() {}

    function onStop() {}

    function getInitialView() {
        return [new GF3Kanoo()];
    }
}
